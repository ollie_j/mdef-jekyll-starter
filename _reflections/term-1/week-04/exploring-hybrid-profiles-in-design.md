---
title: Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---
The Hybrid Profiles week built up towards to the formation of a vision statement that will give me guidance during the masters. There were various activities and exercises carried out during the week which began with visiting some hybrid profile designers in Barcelona and identifying the points of attraction to each. From this I started to build a profile of the qualities that define me as a designer, and how I can use the course modules this term in order to advance the qualities I had identified. Following the creation of a personal development plan, I used some texts which provided insight into how it is possible to achieve a strong sense of direction during a project. This series of reflective exercises helped form a vision which will guide me throughout the rest of the term and something I will refer back to and develop as the year progresses.

### Domestic Data Streamers

Domestic Data Streamers are a company that in essence connect information and people. Their work makes the everyday hidden data that has become a ubiquitous part our lives and transform it into something visible, creating narratives as a means of bringing new meanings and levels of understanding to their subject matter. They present and think of data as something which can be learnt from and to be used as a tool, not this abstract entity which just exists, investigating the ‘why’ not just the ‘what’ of everything that surrounds data.

Their work encompasses wide ranging contemporary issues in society and they have collaborated with a breadth of international brands and organisations including [UNICEF](http://domesticstreamers.com/case-study/general-assembly-united-nations/) and [Spotify](https://domesticstreamers.com/case-study/the-timekeeper-spotify-data-analytics/) to name but a few. This is reflected in their multidisciplinary team, who take an abstract idea or question and through a process of research, concept development, and production make it something understandable and relatable. An example is when they tackled the question of what design should do in their project [Design Does*](https://domesticstreamers.com/case-study/design-does/).

![]({{site.baseurl}}/designdoes.jpg)

Learning about the work of Domestic Data Streamers was a really positive experience and I am really glad to have been introduced to them as they will provide a useful point of reference for me in multiple ways. There are many aspects of their work and approach which I feel I can learn from and apply to my work, not only during the duration of the masters but also in my professional life in general. I have already touched on Domestic’s approach and the main aspects of their work with resonate with me the most can be summarized as:

* Their underlying theme of adding transparency to information.
* Their process, approach and attitude in general.
* Multidisciplinary team that forms Domestic.
* User engagement techniques and understanding of behaviour.
* Work presentation and documentation.

The way Domestic turn something which is invisible such as data, which has point of reference in the physical world and make it understandable and informative is something I am really interested in doing in my own work and is the aspect of their work which resonates with me the most. Articulating something quite abstract into something physical, visual and interactive is a space I want to explore further due to my interest in increasing accessibility and understanding of new technologies. Early projects that Domestic undertook were very conceptual and it was interesting to see that this approach has been maintained through the company’s growth and development as they have worked with larger and international brands.

I also really like how Domestic frame a project as a learning opportunity and their curiosity about whatever subject matter they are dealing with is evident in order to create a work environment of constant personal and professional development. The fact that they rarely reject work, and that a project is never really considered finished demonstrates the idea of continued learning, and I can feel I can learn a lot from this approach. Not only that but knowing how and who to connect with for a particular project whereby more specific expertise is necessary can be really important.

Another area of interest I have is user engagement and participatory processes, which I think Domestic really strongly relate to. For a start their work is based around telling people’s stories, and the human relation to data. As a result a really good understanding of how people interact with one another and human behaviour in general is necessary. Furthermore a lot of their work includes installations with an element of interactivity, where the interactions are recorded or visualised in some way. In a sense they design environments and systems which provide the setting for combining people, emotions and data. From this I can learn a lot from how they create environments in which people feel comfortable in sharing information and engaging with. A really nice example they showed during the workshop was a method used to convey emotions through adding colour to a small figure, so people could communicate their feelings in a nonverbal way.

![]({{site.baseurl}}/figure.jpg)

Over the past year there has been a lot of negativity surrounding data collection, which is why I was surprised to leave the visit to Domestic with a changed view about this subject. They use data collected from people through their interventions and use it in a positive way, shedding light on a subject and changing perceptions. This is something I could try and incorporate in my own projects if I work with creating environments and interventions for people to interact with and meet.

Another quality of the studio that I liked as a more general point was the multidisciplinary team that forms the company. Having a such a broad range of people together means that each of the disciplines can be informed by the others it was evident they believed richer outcomes were possible as a result. I am experiencing this directly during the masters currently, and was one of the reasons I wanted to be a part of the course. Seeing the benefits in a professional setting has encouraged me further of the benefits of working in this way, and I feel strongly about trying to continue working in a similar environment in the future.

A final comment on Domestic’s presentation and documentation of their projects I feel I could bring to my work is the way that they present each project through clear, consistent and succinct videos, such as the video which describes the project they did for the UN.

[![]({{site.baseurl}}/video.jpg)](https://vimeo.com/176458890)

As part of the visit there was an activity to make us think about the hidden data and information in the everyday objects we carry around with us. With the contents of our bags and pockets we were asked to present them in a way which made sense to us and gave them a new meaning. I went through the objects I had and found two items I didn’t realise I was carrying around with me, a torn out page of a sudoku book hidden in the back of my phone and a woven label in my laptop case. It occurred to me that these two seemingly unrelated objects were linked through me acquiring them in Colombia earlier in the year, a friend giving me the sudoku page in case I was ever bored and the woven patch also a gift from a friend from the café I liked to go to in Medellín. I tried to show this connection in a Venn diagram with the outline Colombia as the central zone.

During the exercise I learnt a lot about my classmates, not only the things they carry around with them but why, and the stories behind some of the items. It also made me think about how indecisive I am, carrying around a woven patch for months trying to think of the right thing to sew it to and that maybe that I am a little sentimental, for some reason keeping an intermediate level sudoku on my person which I could easily search for online.

![]({{site.baseurl}}/workshop.jpg)

![]({{site.baseurl}}/break10.png)

### Ariel Guersenzvaig

[Ariel Guersenzvaig](http://www.elisava.net/en/center/professorate/ariel-guersenzvaig),  is an academic at Elisava, Barcelona, who has a fascinating background in both research and industry. He began working in information science but has explored many different areas over his career including human computer interaction, design methodology and ethics in design which is his current area of focus.

His professional and academic life has been shaped by the technological disruptions that began during the mid to late 90s during the web’s infancy. There seems to be a common theme running through Ariel’s career where his professional life and academic pursuits have crossed paths and informed one another, through him following his personal interests exploring different areas initially professionally which he then subsequently explored in various academic settings. Computers have also been a large part of his life and he began by exploring how people interact with content at a time when humans only began using screens.

He discussed his academic and professional development over the last 30 years in reference to four themes: designing, teaching, researching and managing. The amount of time dedicated to each theme has changed over time, with ‘teaching’ rising over the years as he’s become more involved with Elisava whilst ‘designing’ has declined. Researching has steadily grown and seems to be a really important part of his current endeavours. The way he used these themes as a way of reflecting on his own life was really interesting not only as a way of understanding the different forces that have shaped his career, but also because it serves as a reference for forming personal reflections, the main activity of the week.

I feel can learn a lot from Ariel’s attitude in general as he has allowed himself to be led by personal interests, and acted on them which has led to many opportunities. As mentioned an initial interest of his was human interaction with computers, so he started a company and worked in this area for a number of years. Another area which  he explored was decision making in design, and after a number of years generally reading about the subject he completed a PhD on the subject. In his thesis he explored how designers carry out research and the act of ‘designing’, challenging traditional notions of decision making in design.

I think my interest in participatory design can be informed and developed from Ariel’s research into decision making. Ariel talked about the ‘wicked problem’ concept which views design problems as wicked because they can’t be dealt with in traditional ways, and that solutions are not right or wrong, just better or worse. I think this concept relates to participatory design given its history of including the user in the design process with various levels of efficacy. The idea of defining problems is also relevant and more research and reading into his work will be necessary in order to explore its relevance further.

Some research on Ariel led me to a book entitled [Developing Citizen](http://www.elisava.net/en/center/news/presence-ariel-guersenzvaig-prestigious-book-developing-citizen), which contains a case study of a project he talked about during the workshop, the [Youth Facilities of Llagostera](http://www.elisava.net/en/center/current-affairs/news/elisava-designs-future-space-young-people) discussed in the chapter on participatory design. The book deals with different themes such as social design, ethics, sustainability and more.

![]({{site.baseurl}}/developingcitizen2.jpg)

Ariel’s current area of research is ethics, namely how it links to design and as an activity we had a discussion about whether or not killer robots should be permitted to introduce us to the way to tackle this kind of question. This is a subject Ariel knows well and he has had an article published on the issue entitled [Autonomous Weapon Systems: Failing the Principle of Discrimination](https://ieeexplore.ieee.org/document/8307136), so it was really interesting to debate the issue with him as a group. We discussed the problems faced when tracing the chain of responsibility imaging a scenario if a killer robot were to kill a wrong target, relating the question generally in relation to just war theory.

Ariel believes ethics is an important field to study due to the rapid technological change which is occurring. He spoke about how one can be guided by intuition when expert level of knowledge is reached in given field, and how this intuition can guide a particular professional in decision-making. Relating this notion to technological advancements, most people do not have the necessary knowledge to have intuition about the problems faced by these advancements, so acquiring the necessary knowledge in order to get to grips with a particular topic and debate is critical. For instance how can you have intuition about problems faced by machine learning, or malicious uses of AI with no knowledge about them? As a result, if constructive discussions about technology and ethical issues are to happen experts are needed.

I have an interest in these kinds of debates surrounding technology and now I have much more of a desire to engage and read more in order to participate and try to answer the emerging questions posed related to ethics and technology. One of my interests I am keen to explore in this masters is increasing technological literacy and understanding, so opening up debates such as whether or not killer robots should be allowed, among the myriad of other emerging ethical issues could be an interesting space to explore. The need to have these conversations, engagement and critical thinking about these issues is only increasing so thinking about ways of engaging people in interesting ways to learn and debate these issues could be something that I would like to explore. The work of Domestic Data Streamers I think also strongly relates to this theme, through designing environments for interactions and communicating the abstract ideas in a clear and concise way.

![]({{site.baseurl}}/break10.png)

### Sara de Ubieta

The final visit to a hybrid profile was to [Sara de Ubieta’s](http://www.deubieta.com/) studio, located in an industrial part in the east of Barcelona. It was interesting to see this area and how it differed from Poblenou, which is where I assumed all artists and designers must be operating in Barcelona. Sara is a shoe designer with an architectural background, who started shoe making during her studies as an architecture student but turned it into a profession following the 2008 financial crash. Her studio is filled with shoes, tools and I felt complemented the other visits really well through exposing us to a designer who operates more as an individual as opposed to someone who works in a university or as part of a larger design studio.

![]({{site.baseurl}}/studio2.png)

In the early part of her career as a shoe maker she worked mainly designing and producing shoes, learning the craft and techniques necessary and understanding how materials work and come together physically on a shoe. Sara described how she began working with leather and told us about the characteristics of the material and the limitations faced due to the leather being stretched and how this produces a lot of waste material.

It was working with leather that triggered her first explorations into what consists of one of her current primary practices: research. Sara being a vegan felt guilty using leather as her shoe making material of choice, and the amount of waste it produced forced her to use the material in a new way. She developed a technique using offcuts instead of the larger pieces of leather normally used in shoemaking which are stitched together.

This approach called for a new method of attaching and combining leather onto a shoe as the offcuts were too small to be attached using traditional methods.The result has an amazing quality to it, with the shoe appearing like a wearable leather collage. This exploration and ethos of experimentation has been an important part of her work since and she has tried and tested multiple ways of making shoes with a range of other materials. The exploratory nature of this work had a focus on using materials in new ways, looking at more sustainable ways of shoe making and production. Although the resulting shoes of these studies do not necessarily get produced commercially, the idea of using materials in new ways for shoe making has done. An example of this is the use of offcuts of architectural wooden beams as the heel of a woman’s shoe.  

![]({{site.baseurl}}/shoes.png)

During the workshop we were able to have a taster of Sara’s process through making our own shoes using recycled architectural insulation. It was a really fun activity and each group during the workshop used the material in totally different ways which was really interesting to see. The group I was in worked with the material from the offset and began making string which could be weaved together resulting a kind of slipper.

![]({{site.baseurl}}/shoemaking.png)

Working in this hands-on playful manner was one of the things I most liked about Sara’s work as was her dedication to shoe making and experimenting with different techniques. This way of working has resulted in her having a really strong identity as a designer and the fact that she is not bogged down with the pressures of industry have afforded her the opportunity to develop her brand through the act of designing which I really admire. As a result she is now in the position where she can mix her personal practice as a researcher and her commercial work and can find the right balance that suits her. Having a foot in education and teaching makes her everyday activities diverse and I think is a really nice mix for a designer to have.

![]({{site.baseurl}}/deubieta.jpg)

![]({{site.baseurl}}/break10.png)

### Building my Profile

The visits to each of the hybrid profiles helped me to identify points of attraction through comparing myself to each of them. The main points have been made in the text above, but a summary of the main points of attraction in terms of skills, knowledge and attitude are as follows:

![]({{site.baseurl}}/ask.jpg)

From these points of attraction I was able to begin reflecting on who I am, and who I want to be in terms of skills, knowledge and attitude. I have ranked these points relating to if I feel I have achieved them or not, with number 1 I feel the point I feel I have most achieved.

![]({{site.baseurl}}/list.jpg)

I feel from point 4 onwards I have some way to go before achieving them. However I feel that they are all achievable during the masters, and that the different modules and their diversity can help me to do so. I will go through each point, and reflect on how I can use the first term's modules can help me achieve them by bridging the gap between what I know now and what don’t know. I will also look back at the previous weeks already completed and how they have helped develop my learning.

  *I like being surrounded by and working with people from a variety of disciplines.*

Being part of the masters currently means I have already achieved this. Visiting Domestic really made me realise the value of this in a professional setting. I need to continue to make use of and learn from the variety of skills and vast knowledge of the group on the course, which relates to every module during this term.

  *I like developing a project in a hands-on exploratory manner.*

I was able to work in this way during *Design For the Real Digital World* (week 3) and feel confident making and using some of the tools in the Fab Lab. I will develop these skills in during *The Way Things Work* (week 7) and *From Bits to Atoms* (week 11), focusing on digital fabrication and physical computing.

  *I like being able to change perceptions and add understanding to a particular theme through graphic design / model making.*

During *Biology Zero* (week 2) I tried to apply this thinking, and can use *Designing for Extended Intelligence* (week 6) as opportunity to understand some of the keys concepts of AI and communicate them in a visually or through physical models.

  *I like exploring and developing methods of user engagement and participatory design.*

*Hybrid Profiles* (week 4) helped develop some ideas and introduce me to some projects which I can use as reference material. *Living with Ideas* (week 8) will also provide the opportunity to test some of these ideas.

  *I like to try out my ideas in the real world making links with local businesses and the community.*

*MDEF Bootcamp* (week 1) introduced me to the resources and local businesses that I could collaborate with. *Living with Ideas* (week 8) will provide the opportunity to make links with these groups and businesses.

  *I like being able to let personal interests influence my professional work.*

*Navigating the Uncertainty* (week 5) will give me a chance to reflect on my current thinking and do further reading and research to develop my area of interest. *Designing with Extended Intelligence* (week 6) will allow me to learn a considerable amount more about AI and could influence the theme of my research.

  *I like being able to understand and communicate ethical issues surrounding new technologies through essays.*

Similarly to point 6, *Navigating the Uncertainty* (week 5) will provide the opportunity for extended research and the output as a reflection for this week could take the form of an essay. During *An Introduction to Futures* (week 10) there may be the opportunity to explore ethical issues associated with future technologies, as there will also be during *Designing with Extended Intelligence* (week 6) for exploring the ethical issues surrounding AI.

  *I like communicating the key concepts and ideas of a project through film.*

I am interested in using film as a way of presentation and documentation and documentation could be done through film for each of the weeks. For using film for presentation I think *Engaging Narratives* (week 9) could be a week to do this, as will *Design Dialogues* (week 12), for the presentations at the end of the term.

  *I like using participatory design methods to increase accessibility to new technologies.*

*An Introduction to Futures* (week 10) will enable me to learn more about new technologies as well as guide my overall area interest for the research studio. *Living with Ideas* (week 8) will also be an opportunity to test this overall theme.

  *I like making information more understandable and accessible through interactive, three dimensional installations linking the digital and physical world.*

*Designing for Extended Intelligence* (week 6) and *An Introduction to Futures* (week 10) will help me to develop the theoretical aspects of this aim, whilst *The Way things Work* (week 7) and *From Bits to Atoms* (week 11) will help develop practical skills and finally *Design Dialogues* (week 12) could be a chance to try and present this idea.

Reflecting on these points has been a really useful exercise and I find it exciting to know I am able to shape my own learning, acquire the specific skills and knowledge I want to in order to aid my areas of interest. As a way of gaining perspective for a project in general, as a group during a workshop this week we analysed some articles relating to [vision](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products), [experience](https://dl.acm.org/citation.cfm?id=2399045) and [reflection](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge). This was a really important exercise as having clarity of thought when working with issues that are uncertain, complex and changing can provide some stability and direction.

The main takeaway from the essay relating to vision was the idea of having a flexible working method, which is in constant evolution and enables you to develop and learn from mistakes as you work through a project. The model described in the essay provides an alternative to a rigid model (formal analysis phase followed by a creative conceptual phase) through proposing a more open and flexible model where design acts as a generator of knowledge and is carried out in parallel to analysis. Something else I like about the model proposed is the idea of working in small steps, and the adaptive nature of the model which enables you to move between the different phases, or work on them simultaneously to avoid dead-ends and frustration. The idea of speculating whilst relating a project to its context, or even pursue in the real world is one I feel I would like to incorporate into my project. I think this mixture of speculation connected to reality is particularly important to do when considering multiple and different emergent futures.

Some other key ideas from the other texts was using precedents as a way to develop a project and critiquing existing work to get a sense of what might be a good way to approach a particular issue. Another idea that really resonated with me was becoming a part of the environment or community in which you are designing for in order to gain new perspectives that would otherwise not be possible.

![]({{site.baseurl}}/break10.png)

### Vision

The week’s activities had been starting the process of reflection relating to specific areas of individual interest and building up to the proposal of a vision. The reflections this week have really helped develop my thinking, although I have found it quite challenging at times. Part of the reason I found it difficult is because I have lots of interests I want to explore, and the idea of narrowing down and making a specific vision is something I find daunting. The visits to the hybrid profiles helped me with this way of thinking and the importance of having a clear and strong vision. In just a brief encounter with each of the designers it was really easy to see how their focus on a particular theme or idea has resulted in really interesting, fulfilling and varied work. It was also reassuring to see how a vision isn’t something that is fixed and can change and evolve over time. Each designer holding the identity of hybrid designer demonstrates this.

I also have often been passive and avoided reflecting on my interests so critically throughout my previous studies and professional life precisely because it is a hard thing to do. The exercises this week are something I think will be necessary to carry on doing throughout the year and beyond the masters, in order to reflect on the direction I am going in and to make the most of the opportunities that present themselves.

My vision can currently can be summarized as:

  ***To develop and use participatory processes as a means of providing users with understanding of, and access to emergent technologies and their impact on individuals and society as a whole.***

As a starting point I am interested in exploring and applying the learning by doing methodology to the participatory processes I aim to develop. Furthermore I view the development of these participatory processes to have applications beyond improving knowledge about new technologies, but this is an area I would like to investigate initially.

![]({{site.baseurl}}/break10.png)

### References

[Designing for the unknown : a design process for the future generation of highly interactive systems and products](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products)

[Annotated Portfolios and Other Ways of intermediate-Level Knowledge](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge)


[Designing for, with or within: 1st, 2nd and 3rd person points of view on designing for systems](https://dl.acm.org/citation.cfm?id=2399045)
