---
layout: home
---
<font size="6">This site is dedicated to the documentation of the Master in Design for Emergent Futures programme held at IaaC and Elisava School of Design & Engineering, Barcelona. A series of reflections form this documentation which record learnings, activities and feelings captured throughout the year.</font>


<u>Documentation Samples</u>

![](images/mapping.jpg)

*Mapping Poblenou (MDEF Bootcamp), October 2018*

![](images/bio.png)

*Biology Zero, October 2018*

![](images/render.jpg)

*Design For the Real Digital World, October 2018*

![](images/shoes.png)

*Visit to Sara De Ubieta's studio (Hybrid Profiles), October 2018*
